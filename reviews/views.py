from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from book_exchanger.forms import *
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.forms import ModelForm
from django.contrib.auth.views import LoginView
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q, F
from django.views import View
from django.views.generic import TemplateView, ListView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from reviews.forms import *


# Create your views here.
@login_required(login_url=reverse_lazy('book_exchanger:login'))
def add_review(request, book_id):
    if request.method == "POST":
        form = BookReviewForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            description = form.cleaned_data['description']
            book_id = form.cleaned_data['book_id']
            BookReview.objects.create(title=title, description=description, book_id=book_id,
                                      author=request.user)
            return redirect('book_exchanger:success')
    else:
        form = BookReviewForm()
        return render(request, "review_form.html", {'form': form, 'book_id': book_id})


class BookReviewListView(ListView):
    model = BookReview
    context_object_name = 'book_review_list'
    template_name = 'book_reviews.html'
    queryset = BookReview.objects.all()
