from django.db import models
from book_exchanger.models import *


# Create your models here.
class Review(models.Model):
    description = models.TextField(max_length=200)
    title = models.CharField(max_length=20, null=True)
    author = models.ForeignKey(User, related_name='reviews', on_delete=models.CASCADE, default=1)

    class Meta:
        db_table = "review"


class BookReview(Review):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "book review"
        db_table = "book_review"


class UserReview(Review):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "user review"
        db_table = "user_review"


class ReviewComment(models.Model):
    review = models.ForeignKey(Review, on_delete=models.CASCADE)
    text = models.TextField(40)

    class Meta:
        verbose_name = "review comment"
        db_table = "review_comment"
