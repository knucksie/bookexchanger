from django import forms
from book_exchanger.models import *
from django.utils.translation import gettext_lazy as _
from reviews.models import *


class BookReviewForm(forms.Form):
    description = forms.CharField(required=True, widget=forms.Textarea)
    book_id = forms.IntegerField(widget=forms.HiddenInput)
    title = forms.CharField(required=True)


class ReviewCommentForm(forms.ModelForm):
    class Meta:
        model = ReviewComment
        fields = ['text']
