from django.contrib import admin
from django.urls import path
from book_exchanger.views import *
from django.contrib.auth.views import LogoutView
from reviews.views import *
app_name = 'reviews'
urlpatterns = [
    path('all/', BookReviewListView.as_view(), name='all'),
    path('add_review/<int:book_id>/', add_review, name='add_review'),

]
