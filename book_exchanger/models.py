from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from enum import Enum


class RequestStatus(Enum):
    APPROVED = "Одобрено"
    DECLINED = "Отказано"
    PROCESSING = "На оформлении"
    ON_REVIEW = "На рассмотрении"


def profile_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT / user_<id>/<filename>
    return 'profile_pictures/profile_{0}'.format(instance.id)


# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile', null=True)
    name = models.CharField(max_length=10, null=True)
    surname = models.CharField(max_length=10, null=True)
    bio = models.TextField(max_length=100, null=True)

    class Meta:
        db_table = "user_profile"


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Author(models.Model):
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)

    def __str__(self):
        return f'{self.name} {self.surname}'


class Book(models.Model):
    title = models.CharField(max_length=20)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name='books')

    def __str__(self):
        return self.title


class BookInstance(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name='instances')
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='books')

    class Meta:
        verbose_name = "book instance"
        db_table = "book_instance"


class BookRequest(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='incoming_requests')
    requestant = models.ForeignKey(User, on_delete=models.CASCADE, related_name='outcoming_requests')
    books = models.ManyToManyField(BookInstance)
    status = models.CharField(
        max_length=20,
        choices=[(tag, tag.value) for tag in RequestStatus]  # Choices is a list of Tuple
    )

    class Meta:
        verbose_name = "book request"
        db_table = "book_request"
