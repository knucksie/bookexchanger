from rest_framework import viewsets, permissions
from book_exchanger import models
from book_exchanger import serializers


class BookInstanceViewSet(viewsets.ModelViewSet):
    queryset = models.BookInstance.objects.all()
    serializer_class = serializers.BookInstanceSerializer


class AuthorViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    queryset = models.Author.objects.all()
    serializer_class = serializers.AuthorSerializer
