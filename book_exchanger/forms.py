from django import forms
from book_exchanger.models import *
from django.utils.translation import gettext_lazy as _


class UserForm(forms.ModelForm):
    password_again = forms.CharField(max_length=32, widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class ProfileForm(forms.Form):
    name = forms.CharField(label='Имя')
    surname = forms.CharField(label='Фамилия')
    bio = forms.CharField(widget=forms.Textarea(), label='О себе')


class LoginForm(forms.Form):
    username = forms.CharField(label="Имя пользователя", required=True)
    password = forms.CharField(max_length=32, widget=forms.PasswordInput, required=True, label="Пароль")

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(LoginForm, self).__init__(*args, **kwargs)


class BookSearchForm(forms.Form):
    title = forms.CharField(required=True, label='Введите название книги', max_length=40)


class BookForm(forms.Form):
    title = forms.CharField(label='Название книги', required=True)
    author = forms.ModelChoiceField(queryset=Author.objects.all())
