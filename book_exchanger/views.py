from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from book_exchanger.forms import *
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.forms import ModelForm
from django.contrib.auth.views import LoginView
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q, F
from django.views import View
from django.views.generic import TemplateView, ListView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
def signup(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect(reverse('book_exchanger:index'))
    else:
        form = UserForm()
    return render(request, 'registration.html', {'form': form})


def index(request):
    return render(request, 'index.html')


@login_required(login_url=reverse_lazy('book_exchanger:login'))
def search_books(request):
    title = request.POST.get("q")
    if title is not None:
        books = Book.objects.filter(Q(title=title) | Q(title__contains=title))
        return render(request, 'search_result.html', {'books': books})
    else:
        return render(request, 'error.html', {'error': "Введён пустой поисковой запрос."})


@login_required(login_url=reverse_lazy('book_exchanger:login'))
def get_book_owners(request, book_id):
    instances = BookInstance.objects.filter(book_id=book_id)
    return render(request, 'owners.html', {'instances': instances})


@login_required(login_url=reverse_lazy('book_exchanger:login'))
def request_book(request, instance_id):
    try:
        instance = BookInstance.objects.get(pk=instance_id)
        books_request, created = BookRequest.objects.get_or_create(requestant_id=request.user.id,
                                                                   owner_id=instance.owner.id,
                                                                   defaults={"status": RequestStatus.PROCESSING})
        books_request.books.add(instance)
        return redirect(reverse('book_exchanger:success'))

    except ObjectDoesNotExist:
        error = "Упс, похоже владелец избавился от данного экземляра :("
        return render(request, "error.html", {'error': error})


@login_required(login_url=reverse_lazy('book_exchanger:login'))
def add_book_instance(request, book_id):
    BookInstance.objects.create(book_id=book_id, owner_id=request.user.id)
    return redirect(reverse('book_exchanger:success'))


class SuccessVew(TemplateView):
    template_name = "success.html"


class UserBookList(LoginRequiredMixin, View, ):
    login_url = reverse_lazy('book_exchanger:login')
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):
        books = BookInstance.objects.filter(owner_id=request.user.id)
        return render(request, 'mybooks.html', {'books': books})


@login_required(login_url=reverse_lazy('book_exchanger:login'))
def add_book(request):
    if request.method == 'POST':
        form = BookForm(request.POST)
        if form.is_valid():
            book, created = Book.objects.get_or_create(title=form.cleaned_data['title'],
                                                       author=form.cleaned_data['author'])
            instance, created = BookInstance.objects.get_or_create(book=book, owner=request.user)
            return redirect(reverse('book_exchanger:success'))
    else:
        form = BookForm()
    return render(request, "add_book.html", {'form': form})


class BookInstanceDeleteView(LoginRequiredMixin, DeleteView, ):
    model = BookInstance
    success_url = reverse_lazy('book_exchanger:success')
    login_url = reverse_lazy('book_exchanger:login')
    redirect_field_name = 'redirect_to'


class IncomingRequestsView(LoginRequiredMixin, View, ):
    login_url = reverse_lazy('book_exchanger:login')
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):
        requests = BookRequest.objects.filter(owner_id=request.user.id)
        return render(request, 'incoming.html', {'requests': requests})


class OutcomingRequestsView(LoginRequiredMixin, View, ):
    login_url = reverse_lazy('book_exchanger:login')
    redirect_field_name = 'redirect_to'

    def get(self, request, *args, **kwargs):
        requests = BookRequest.objects.filter(requestant_id=request.user.id)
        return render(request, 'outcoming.html', {'requests': requests})


@login_required(login_url=reverse_lazy('book_exchanger:login'))
def approve_request(request, request_id):
    BookRequest.objects.get(pk=request_id).update(status=RequestStatus.APPROVED)
    return redirect(reverse('book_exchanger:success'))


@login_required(login_url=reverse_lazy('book_exchanger:login'))
def decline_request(request, request_id):
    BookRequest.objects.get(pk=request_id).update(status=RequestStatus.DECLINED)
    return redirect(reverse('book_exchanger:success'))


@login_required(login_url=reverse_lazy('book_exchanger:login'))
def process_request(request, request_id):
    BookRequest.objects.get(pk=request_id).update(status=RequestStatus.ON_REVIEW)
    return redirect(reverse('book_exchanger:success'))


@login_required(login_url=reverse_lazy('book_exchanger:login'))
def edit_profile(request):
    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            profile = Profile.objects.create(name=form.cleaned_data['name'],
                                             surname=form.cleaned_data['surname'],
                                             bio=form.cleaned_data['bio'])
            request.user.profile = profile
            request.user.save()
            return redirect(reverse('book_exchanger:success'))
    else:
        form = ProfileForm()
    return render(request, "edit_profile.html", {'form': form})


@login_required(login_url=reverse_lazy('book_exchanger:login'))
def view_profile(request):
    return render(request, 'profile.html')
