from django.contrib import admin
from django.urls import path, re_path
from book_exchanger.views import *
from django.contrib.auth.views import LoginView, LogoutView
from rest_framework import viewsets

app_name = 'book_exchanger'
urlpatterns = [
    path('signup/', signup, name='signup'),
    path('index/', index, name='index'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('outcoming/', OutcomingRequestsView.as_view(), name='outcoming'),
    path('incoming/', IncomingRequestsView.as_view(), name='incoming'),
    path('search/', search_books, name='search'),
    path('my_books/', UserBookList.as_view(), name='my_books'),
    path('get_book_owners/<int:book_id>/', get_book_owners, name='get_book_owners'),
    path('add_book_instance/<int:book_id>/', add_book_instance, name='add_book_instance'),
    path('success/', SuccessVew.as_view(), name='success'),
    path('add_book/', add_book, name='add_book'),
    path('request_book_instance/<int:instance_id>', request_book, name='request_book_instance'),
    re_path(r'^book_instance/(?P<pk>[\w-]+)/$', BookInstanceDeleteView.as_view(),
            name='book_delete'),
    path('decline_request/<int:request_id>', decline_request, name='decline_request'),
    path('approve_request/<int:request_id>', approve_request, name='approve_request'),
    path('process_request/<int:request_id>', process_request, name='process_request'),
    path('edit_profile/', edit_profile, name='edit_profile'),
    path('profile/', view_profile, name='profile')
]
