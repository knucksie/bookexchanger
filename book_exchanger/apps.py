from django.apps import AppConfig


class BookexchangerConfig(AppConfig):
    name = 'book_exchanger'
