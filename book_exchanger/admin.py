from django.contrib import admin
from book_exchanger.models import *
from django.contrib.auth.models import User

# Register your models here.
admin.site.register(Book)
admin.site.register(BookInstance)
admin.site.register(Profile)
admin.site.register(Author)
admin.site.register(BookRequest)


