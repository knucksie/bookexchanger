from rest_framework import routers
from book_exchanger import api_views

router = routers.DefaultRouter()
router.register(r'book_instance', api_views.BookInstanceViewSet)
router.register(r'author', api_views.AuthorViewSet)